## About

GitHub Pages is a Cloudron app for hosting static web pages for GitHub users, user blogs, project documentation or even whole books.

It is integrated with the Jekyll software for static web site and blog generation. The Jekyll source pages for a web site can be stored as a Git repository inside the app, and when the repository is updated the GitHub Pages servers will automatically regenerate the site.

## Jekyll Support

This app uses the GitHub pages gem and as such only supports the Jekyll version and Jekyll
plugins that GitHub supports for it's Pages feature.

See [Dependency versions](https://pages.github.com/versions/) for the version list.

