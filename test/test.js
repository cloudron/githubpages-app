#!/usr/bin/env node

/* jshint esversion: 8 */
/* global describe, before, after, it, xit */

'use strict';

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    superagent = require('superagent');

describe('Application life cycle test', function () {
    this.timeout(0);

    const REPO_DIR = '/tmp/page';
    const LOCATION = process.env.LOCATION || 'test';
    const EXEC_OPTIONS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    let app;
    const SSH_PORT = 29418;
    const newFilename = 'newfile.html';

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function isPageReachable() {
        const response = await superagent.get(`https://${app.fqdn}/`);
        expect(response.status).to.eql(200);
        expect(response.text.indexOf('Super fast Jekyll theme')).to.not.eql(-1);
    }

    async function returns404Page() {
        const response = await superagent.get(`https://${app.fqdn}/random`).ok(() => true);
        expect(response.status).to.eql(404);
        expect(response.text.indexOf('misplaced that URL')).to.not.eql(-1);
    }

    async function returnDirIndexNoTrailingSlash() {
        const response = await superagent.get(`https://${app.fqdn}`);
        expect(response.statusCode).to.eql(200);
        expect(response.text.indexOf('Super fast Jekyll theme')).to.not.eql(-1);
    }

    async function htmlExtension() {
        const response = await superagent.get(`https://${app.fqdn}/about`);
        expect(response.statusCode).to.eql(200);
        expect(response.text.indexOf('seo optimized')).to.not.eql(-1);
    }

    async function prefersExtensionOverDir() {
        const response = await superagent.get(`https://${app.fqdn}/projects`);
        expect(response.statusCode).to.eql(200);
        expect(response.text.indexOf('Summary of project')).to.not.eql(-1);
    }

    function pushKey() {
        execSync(`chmod g-rw,o-rw ${__dirname}/id_ed25519`);
        execSync(`cloudron push --app ${LOCATION} ${__dirname}/id_ed25519.pub /app/data/ssh/authorized_keys`, EXEC_OPTIONS);
        execSync(`cloudron exec --app ${LOCATION} -- chmod 600 /app/data/ssh/authorized_keys`);
    }

    function pushFileSSH() {
        const env = Object.create(process.env);
        env.GIT_SSH = __dirname + '/git_ssh_wrapper.sh';
        env.GIT_SSH_VARIANT='ssh';
        fs.writeFileSync(`${REPO_DIR}/${newFilename}`, `Hello world . ${new Date().toString()}`);
        execSync(`git add ${newFilename}`, { env, cwd: REPO_DIR });
        execSync(`git commit -a -mx`, { env, cwd: REPO_DIR });
        try {
            execSync(`git remote add page ssh://git@${app.fqdn}:${SSH_PORT}/app/data/repo.git`, { env, cwd: REPO_DIR });
        } catch (e) {
            console.log('remote probably already exists');
        }
        execSync(`git push page master --force`, { env, cwd: REPO_DIR });
    }

    async function checkPushedSSHFile() {
        const response = await superagent.get(`https://${app.fqdn}/${newFilename}`);
        expect(response.statusCode).to.eql(200);
        expect(response.text.indexOf('Hello world')).to.not.eql(-1);
    }

    async function initRepo() {
        await fs.promises.rm(REPO_DIR, { recursive: true, force: true });
        execSync(`git clone https://github.com/ronv/ultra.git ${REPO_DIR}`);
    }

    before(async function () {
        if (!process.env.USERNAME) throw new Error('USERNAME env var not set');
        if (!process.env.PASSWORD) throw new Error('PASSWORD env var not set');
    });


    after(async function () {
        await fs.promises.rm(REPO_DIR, { recursive: true, force: true });
    });

    xit('build app', function () { execSync('cloudron build', EXEC_OPTIONS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_OPTIONS); });
    it('can get app information', getAppInfo);
    it('init repo', initRepo);

    it('can push page to app', function () {
        execSync(`git config http.postBuffer 1048576`, { cwd: REPO_DIR, stdio: 'inherit' });
        execSync(`git push https://${encodeURIComponent(process.env.USERNAME)}:${encodeURIComponent(process.env.PASSWORD)}@${app.fqdn}/_git/page master --force`, { cwd: REPO_DIR, stdio: 'inherit' });
    });

    it('page is reachable', isPageReachable);

    it('can push SSH key', pushKey);
    it('can git push file via SSH', pushFileSSH);
    it('can open pushed file', checkPushedSSHFile);

    it('returns 404', returns404Page);
    it('uses html extension', htmlExtension);
    it('prefers ext html over dir', prefersExtensionOverDir);
    it('dir index no trailing slash', returnDirIndexNoTrailingSlash);

    it('can restart app', function () { execSync('cloudron restart --app ' + app.id); });
    it('page is reachable', isPageReachable);

    it('can open pushed file', checkPushedSSHFile);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_OPTIONS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw --app ' + app.id));
        execSync('cloudron uninstall --app ' + app.id, EXEC_OPTIONS);
        execSync('cloudron install --location ' + LOCATION, EXEC_OPTIONS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_OPTIONS);
    });

    it('page is reachable', isPageReachable);
    it('returns 404', returns404Page);
    it('uses html extension', htmlExtension);
    it('prefers ext html over dir', prefersExtensionOverDir);
    it('dir index no trailing slash', returnDirIndexNoTrailingSlash);

    it('uninstall app', function () { execSync(`cloudron uninstall --app ${app.id}`, EXEC_OPTIONS); });

    // update
    it('install previous app', function () { execSync(`cloudron install --appstore-id github.pages.cloudronapp --location ${LOCATION}`, EXEC_OPTIONS); });
    it('can get app information', getAppInfo);
    it('init repo', initRepo);

    it('can push page to app', function () {
        execSync(`git config http.postBuffer 1048576`, { cwd: REPO_DIR, stdio: 'inherit' });
        execSync(`git push https://${encodeURIComponent(process.env.USERNAME)}:${encodeURIComponent(process.env.PASSWORD)}@${app.fqdn}/_git/page master --force`, { cwd: REPO_DIR, stdio: 'inherit' });
    });

    it('page is reachable', isPageReachable);

    it('can push SSH key', pushKey);
    it('can git push file via SSH', pushFileSSH);
    it('can open pushed file', checkPushedSSHFile);

    it('can update', function () { execSync('cloudron update --app ' + app.id, EXEC_OPTIONS); });
    it('can configure', function () { execSync(`cloudron configure --app ${app.id} --location ${app.fqdn} -p SSH_PORT=${SSH_PORT}`); });

    it('returns 404', returns404Page);
    it('uses html extension', htmlExtension);
    it('prefers ext html over dir', prefersExtensionOverDir);
    it('dir index no trailing slash', returnDirIndexNoTrailingSlash);

    it('can open pushed file', checkPushedSSHFile);
    it('can git push file via SSH', pushFileSSH);
    it('can open pushed file', checkPushedSSHFile);

    it('uninstall app', function () { execSync(`cloudron uninstall --app ${app.id}`, EXEC_OPTIONS); });
});
