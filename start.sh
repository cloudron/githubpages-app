#!/bin/bash

set -eu

export REPO_PATH="/app/data/repo.git"
export WEBSITE_PATH="/app/data/website"

mkdir -p /run/githubpages /run/sshd /app/data/ssh

[[ ! -f /app/data/ssh/authorized_keys ]] && touch /app/data/ssh/authorized_keys

[[ ! -f /app/data/env.sh ]] && echo -e "#!/bin/bash\n\nexport BUILD_BRANCH=master\n" > /app/data/env.sh

source /app/data/env.sh

if [[ ! -d $REPO_PATH ]]; then
    echo "=> First run, create bare repo"
    mkdir -p $REPO_PATH
    git init --bare $REPO_PATH

    echo "=> Install welcome page"
    rm -rf $WEBSITE_PATH
    mkdir -p $WEBSITE_PATH
    cp /app/code/welcome.html $WEBSITE_PATH/index.html
fi

# SSH_PORT can be unset to disable SSH
if [[ -z "${SSH_PORT:-}" ]]; then
    echo "==> SSH disabled"
    SSH_PORT=29418 # arbitrary port to keep sshd happy
fi

if grep -q "cloudron-welcome-page" $WEBSITE_PATH/index.html; then
    echo "=> Update welcome page"
    sed -e "s,##REPO_HTTP_URL##,${CLOUDRON_APP_ORIGIN}/_git/page," \
        -e "s,##REPO_SSH_URL##,ssh://git@${CLOUDRON_APP_DOMAIN}:${SSH_PORT}/app/data/repo.git," \
        -e "s,##BUILD_BRANCH##,${BUILD_BRANCH}," \
        /app/code/welcome.html > $WEBSITE_PATH/index.html
fi

if [[ ! -f "/app/data/sshd/ssh_host_ed25519_key" ]]; then
    echo "==> Generating ssh host keys"
    mkdir -p /app/data/sshd
    ssh-keygen -qt rsa -N '' -f /app/data/sshd/ssh_host_rsa_key
    ssh-keygen -qt dsa -N '' -f /app/data/sshd/ssh_host_dsa_key
    ssh-keygen -qt ecdsa -N '' -f /app/data/sshd/ssh_host_ecdsa_key
    ssh-keygen -qt ed25519 -N '' -f /app/data/sshd/ssh_host_ed25519_key
else
    echo "==> Reusing existing host keys"
fi

chmod 0600 /app/data/sshd/*_key
chmod 0644 /app/data/sshd/*.pub

sed -e "s/^Port .*/Port ${SSH_PORT}/" /etc/ssh/sshd_config > /run/githubpages/sshd_config

echo "=> Ensure git hook"
sed -e "s,##CLOUDRON_APP_ORIGIN##,${CLOUDRON_APP_ORIGIN},g" \
    -e "s,##BUILD_BRANCH,${BUILD_BRANCH:-master},g" /app/code/pre-receive.template > $REPO_PATH/hooks/pre-receive
chmod +x $REPO_PATH/hooks/pre-receive

echo "=> Ensure permissions"
chown git:git -R $REPO_PATH /run/githubpages /app/data
chmod 600 /app/data/ssh/*

echo "=> Run GitHub pages"
#exec /usr/local/bin/gosu cloudron:cloudron node /app/code/index.js
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i GitHub-pages
