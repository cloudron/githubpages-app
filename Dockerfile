FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code
WORKDIR /app/code

RUN apt-get update && \
    apt-get install -y openssh-server git && \
    rm -rf /etc/ssh_host_* && \
    rm -r /var/cache/apt /var/lib/apt/lists

COPY supervisor/ /etc/supervisor/conf.d/

# https://rubygems.org/gems/github-pages
# renovate: datasource=rubygems depName=github-pages versioning=ruby
ARG GH_VERSION=232

# https://pages.github.com/versions/
RUN gem install --no-document bundler github-pages:$GH_VERSION

COPY package.json /app/code/
RUN npm install

RUN adduser --disabled-login --gecos 'GitHub Pages' git --shell /usr/bin/git-shell
# by default, git account is created as inactive which prevents login via openssh
# https://github.com/gitlabhq/gitlabhq/issues/5304
RUN passwd -d git

RUN ln -s /app/data/ssh /home/git/.ssh

RUN sed -e 's,^logfile=.*$,logfile=/run/githubpages/supervisord.log,' -i /etc/supervisor/supervisord.conf

COPY start.sh index.js pre-receive.template welcome.html /app/code/
COPY sshd_config /etc/ssh/sshd_config

CMD ["/app/code/start.sh"]
