#!/usr/bin/env node

'use strict';

var express = require('express'),
    bodyParser = require('body-parser'),
    zlib = require('zlib'),
    path = require('path'),
    spawn = require('child_process').spawn,
    backend = require('git-http-backend'),
    ldapjs = require('ldapjs'),
    safe = require('safetydance'),
    basicAuth = require('basic-auth'),
    url = require('url');

function exit(error) {
    if (error) console.error(error);
    process.exit(error ? 1 : 0);
}

if (!process.env.CLOUDRON_LDAP_URL) exit('LDAP_URL needs to be set in the environment');
if (!process.env.CLOUDRON_LDAP_USERS_BASE_DN) exit('LDAP_USERS_BASE_DN needs to be set in the environment');
if (!process.env.WEBSITE_PATH) exit('WEBSITE_PATH needs to be set in the environment');
if (!process.env.REPO_PATH) exit('REPO_PATH needs to be set in the environment');

var app = express();

app.set('trust proxy', 1);
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/_git', function (req, res, next) {
    var userInfo = basicAuth(req);
    if (!userInfo) {
        res.header({'WWW-Authenticate': 'Basic realm="GitHub"'});
        return res.sendStatus(401);
    }

    var ldapClient = ldapjs.createClient({ url: process.env.CLOUDRON_LDAP_URL });
    var ldapDn = 'cn=' + userInfo.name + ',' + process.env.CLOUDRON_LDAP_USERS_BASE_DN;

    ldapClient.bind(ldapDn, userInfo.pass, function (error) {
        if (error) return res.sendStatus(403);

        next();
    });
}, function (req, res) {
    var reqStream = req.headers['content-encoding'] == 'gzip' ? req.pipe(zlib.createGunzip()) : req;

    reqStream.pipe(backend(req.url, function (err, service) {
        if (err) return res.end(err + '\n');

        res.setHeader('content-type', service.type);
        console.log('git:', service.action, service.fields);

        var ps = spawn(service.cmd, service.args.concat(process.env.REPO_PATH));
        ps.stdout.pipe(service.createStream()).pipe(ps.stdin);

    })).pipe(res);
});

app.get('/_healthcheck', function (req, res) {
    res.sendStatus(200);
});

// try_files $uri $uri.html $uri/ =404;
// static middleware gives priority to directory over a filenamed .html. so this works around it
app.use(function (req, res, next) {
    let pathname = url.parse(req.url).pathname;
    if (pathname.endsWith('/')) return next();

    // if pathname is a dir, we have to check if pathname.html exists
    let stat = safe.fs.statSync(path.join(process.env.WEBSITE_PATH, pathname));
    if (!stat || !stat.isDirectory()) return next(); // static middleware will do the necessary

    const maybeHtmlFile = path.join(process.env.WEBSITE_PATH, pathname + '.html');
    stat = safe.fs.statSync(maybeHtmlFile);
    if (!stat || !stat.isFile()) return next(); // static middleware will do the necessary

    res.status(200).sendFile(maybeHtmlFile); // serve up $uri.html even if $uri/ exists...
});

// https://fuzzyblog.io/blog/jekyll/2017/10/06/self-hosting-a-jekyll-site.html
app.use(express.static(process.env.WEBSITE_PATH, {
    fallthrough: true, // call next() for file not found. we will return 404.html
    index: [ 'index.html' ],
    dotfiles: 'ignore', // return 404 for dot files
    extensions: [ 'html' ], // check $uri.html if not found
    redirect: true // if dir, redirect to dir/
}));

// try_files $uri $uri.html $uri/ =404;
app.use(function notFound(req, res) {
    res.status(404).sendFile(path.join(process.env.WEBSITE_PATH, '404.html'));
});

app.use(function (error, req, res) {
    res.status(500).sendFile(path.join(process.env.WEBSITE_PATH, '500.html'));
});

app.listen(3000, function () {
    console.log('Listening on port %s', 3000);
    console.log('Using git repo at %s', process.env.REPO_PATH);
    console.log('Serving up directory %s', process.env.WEBSITE_PATH);
});
